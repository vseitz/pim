# Build the double version of PIM

file(GLOB PIM_DOUBLE_SOURCES lib/*.f src/*.f)

# Add the source files to a "PIM-Library" library
# Set the library type based on the ENABLE_SHARED_PIM_LIB flag
if(${ENABLE_SHARED_PIM_LIB})
    message(STATUS "We will build SHARED PIM-Library library")
    add_library(PIM-Library SHARED ${PIM_DOUBLE_SOURCES} ${PIM_MACHINE_CONSTANTS})
    set_target_properties(PIM-Library
        PROPERTIES
        POSITION_INDEPENDENT_CODE ON
        OUTPUT_NAME "PIM-Library"
        DEBUG_POSTFIX "_d"
        MACOSX_RPATH ON
        WINDOWS_EXPORT_ALL_SYMBOLS ON
        )
else()
    message(STATUS "We will build STATIC PIM-Library library")
    add_library(PIM-Library STATIC ${PIM_DOUBLE_SOURCES} ${PIM_MACHINE_CONSTANTS})
    set_target_properties(PIM-Library
        PROPERTIES
        POSITION_INDEPENDENT_CODE OFF
        ARCHIVE_OUTPUT_NAME "PIM-Library"
        DEBUG_POSTFIX "_d"
        )
endif()