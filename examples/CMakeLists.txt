find_package(BLAS REQUIRED)
add_subdirectory(common)
add_subdirectory(sequential)
add_subdirectory(mpi)

